const correctAnswers = ['B', 'B', 'B', 'B'];
const form =document.querySelector('.quiz-form');
const scorePrev = document.querySelector('.score');
const resultNinja = document.querySelector('.ninjaScore');
const result = document.querySelector('.result');

setTimeout(() => {
    alert('you have 2 minutes to answer all questions!');
}, 2000);

form.addEventListener('submit', e => {
    e.preventDefault();

    let score = 0;

    const userAnswers = [form.q1.value, form.q2.value, form.q3.value, form.q4.value];

    //check answers...

    userAnswers.forEach((answer, index) => {
        if(answer === correctAnswers[index]){
            score += 25; 
        }


    });

    scrollTo(0,0);
    // resultNinja.textContent = `${score}%`;
    result.classList.remove('d-none');
        
    // scorePrev.textContent = `You are a  ${score}% ninja`;
    // console.log(score + '%');

    let output = 0;
    const timer = setInterval(() => {
        resultNinja.textContent = `${output}%`;
        if(output === score){
            clearInterval(timer);
        }
        else{
            output++;
        }
    }, 10);


});